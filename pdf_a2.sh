#!/usr/bin/env bash

RECOMMENDED_GS_VERSION="9.23";
REQUIRED_FILES=("AdobeRGB1998.icc" "PDFA_def.ps" "pdfmarks");

# ECHO COLORS
RED=`tput setaf 1`;
GREEN=`tput setaf 2`;
YELLOW=`tput setaf 3`;
NC=`tput sgr0`;

# PARAMS
pdf_source=$1;
pdf_target=$2;

# VERY SIMPLE CHECK IF SOURCE FILE IS PDF/A
is_pdfa()
{
    pdf_file="${1}";
    info="$(pdfinfo -meta ${pdf_file})";
    test_string="xmlns:pdfaid='http://www.aiim.org/pdfa/ns/id/'";
    if [[ ${info} = *"${test_string}"* ]]; then
        return 0
    fi
    return 1
}

# CREATE PDF/A
create_archive_pdf() {
    old_file="${1}";
    new_file="${2}";

    if is_pdfa ${old_file} ;
    then
        echo "${RED}This file ("${pdf_file}") is already PDF/A. Continue ...${NC}";
        return 0;
    fi

    echo ${old_file};
    echo ${new_file};
    gs \
        -dPDFA=3 \
        -dBATCH \
        -dNOPAUSE \
        -sColorConversionStrategy=RGB \
        -dPDFACompatibilityPolicy=2 \
        -sProcessColorModel=DeviceRGB \
        -sDEVICE=pdfwrite \
        -sOutputFile="${new_file}" $PWD/PDFA_def.ps\
        "${old_file}" pdfmarks
}

parameters_processing() {
    # 1. DIR
    # 2. None
    if [ -d "${pdf_source}" ] && [ -z "${pdf_target}" ] ;
    then
        pdf_array=("$(ls "${pdf_source}"*.pdf | grep -v '^d')");
        for item in ${pdf_array[@]};
        do
            name_with_ext="${item##*/}";
            if [[ "${name_with_ext}" == *.archive.pdf ]] ;
            then
                continue
            fi
                new_name="${pdf_source}${name_with_ext%.*}.archive.pdf";
                create_archive_pdf ${item} ${new_name};
        done

    # 1. FILE
    # 2. None
    elif [ -f "${pdf_source}" ] && [ -z "${pdf_target}" ] ;
    then
        name_with_ext="${pdf_source##*/}";
        dirname=$(dirname "${pdf_source}");
        echo "${dirname}";
        if [[ "${name_with_ext}" == *.archive.pdf ]] ;
        then
            return 0
        fi
        new_name="${dirname}/${name_with_ext%.*}.archive.pdf";
        create_archive_pdf ${pdf_source} ${new_name};

    # 1. DIR
    # 2. DIR
    elif [ -d "${pdf_source}" ] &&  [ -d "${pdf_target}" ] ;
    then
        pdf_array=("$(ls "${pdf_source}"*.pdf | grep -v '^d')");
        for item in ${pdf_array[@]};
        do
            name_with_ext="${item##*/}";
            new_name="${pdf_target}${name_with_ext%.*}.pdf";
            create_archive_pdf ${item} ${new_name};
        done

    # 1. FILE
    # 2. FILE (created if not exist)
    elif [ -f "${pdf_source}" ] &&  [ -n "${pdf_target}" ] ;
        if [[ "${pdf_target}" == *.pdf && ! -f "${pdf_target}" ]] ;
        then
            touch "${pdf_target}";
        fi
    then
        create_archive_pdf ${pdf_source} ${pdf_target};

    else
        echo "${RED}Bad parametrs! Must be file, dir, file file or dir dir${NC}";

     fi
}

# GHOSTSCRIPT VERSION RECOMMENDATION
check_gs_version() {
    version="$(gs --version)";
    if [ "${version}" != "${RECOMMENDED_GS_VERSION}" ];
    then
        echo "${YELLOW}Your gs version(${version}) is not recommenden(expected version ${RECOMMENDED_GS_VERSION}). Problems may occur ...${NC}";
        exit 1;
    fi
}

# FOREACH ARRAY AND CHECK REQUIRED ELEMENT
element_exists() {
  local e match="$1"
  shift
  for e;
  do
    [[ "$e" == "$match" ]] && return 0;
  done
  return 1
}

# CHECK REQUIRED FILES
check_required_files() {
    working_dir_files=($(ls | grep -v '^d'));
    for item in ${REQUIRED_FILES[@]};
    do
        if ! element_exists ${item} ${working_dir_files[@]};
        then
            echo "${RED}All these files are required - ${REQUIRED_FILES[@]}${NC}";
            exit 1;
        fi
    done
}

check_gs_version
check_required_files
parameters_processing
