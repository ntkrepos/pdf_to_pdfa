#!/usr/bin/env bash

image_source=$1;
image_target=$2;

is_jpylyzer_installed() {
    if ! type jpylyzer > /dev/null; then
        echo "JPYLYZER is not installed. Please install (pip install jpylyzer)";
        exit 1;
    fi
}

is_jp2() {

    jp_file="${1}";

    info="$(jpylyzer ${jp_file})";
    test_string="<isValidJP2>True</isValidJP2>";

    if [[ ${info} = *"${test_string}"* ]]; then
        return 0
    fi
    return 1

}

convert_image_to_jp() {

	source_image="${1}";
	target_image="${2}";

	if is_jp2 ${source_image} ;
	then
		echo "Source is jpeg2000";
		return 0;
	fi

	convert "${source_image}" "${target_image}";

	if ! is_jp2 ${target_image} ;
	then
		echo "Convert failed";
		echo "Source file - ${source_image}, target file - ${target_image}";
		return 1;
	fi

}

get_rec_id () {
  # Params
  doc_id="${1}";
  rec_id=$(python /opt/invenio/bin/bibdocfile --get-info --docids=${doc_id} | grep -Po '(?<=\/record\/).*(?=\/files\/)');
  echo "${rec_id}";

}

append_to_record () {
    # Params
    doc_id="${1}";
    jpeg_name="${2}";

    # TODO: NEED CHECK!
    # bibdocfile cmd.
    python /opt/invenio/bin/bibdocfile --append ${jpeg_name} --docids=${doc_id} --with-flags='CONVERTED' --yes-i-know;
}

get_jpeg_name () {
    doc_ide="${1}";

    patern=("jpg:name" "jpg;big:name" "jpg;small:name" "jpeg:name" "jpeg;big:name" "jpg;small:name");
    for tag  in ${patern[@]};
    do
        name=$(python /opt/invenio/bin/bibdocfile --get-info --docids=${doc_id} | grep -Po '(?<=('${tag}'=)).*');
        if [[ ! ${name} == "" ]] ;
        then
            echo "${name}";
            return 0;
        fi
    done
    return 1;

}

is_contained_jp2 ()
{
    rec_id="${1}";
    info="$(python /opt/invenio/bin/bibdocfile --get-info --recids=${rec_id})";

    test_string="jp2:name";

    if [[ ${info} = *"${test_string}"* ]]; then
        return 0
    fi
    return 1
}

list_pdf ()
{
    # Filter only jpeg
    all_jpg=($(find /opt/invenio/var/data/files/ -name "*.jpg;*" -or -name "*.jpeg;*"));
    for jpg in "${all_jpg[@]}";
    do

        doc_id=$(dirname ${jpg} | sed 's#.*/##');
        doc_path=$(dirname ${jpg});
        jpeg_name=$(get_jpeg_name ${doc_id});
        rec_id=$(python /opt/invenio/bin/bibdocfile --get-info --docids=${doc_id} | grep -Po '(?<=\/record\/).*(?=\/files\/)');

        # TODO: PLEASE, BETTER ME
        # Skips all images if the record contains at least one jp2
        # Ugly, but prevents the creation of duplicate
        if  [[ ! "${rec_id}" == "" ]] ;
        then
            if is_contained_jp2 ${rec_id} ;
            then
                continue;
            fi
        else
            continue;
        fi


        if [[ ! "${jpeg_name}" == "" ]] ;
        then
            echo "Work on ${jpeg_name} in record id ${rec_id} ";
        else
            continue;
        fi

        # Build full path and name
        new_file="/tmp${doc_path}/${jpeg_name%.*}.jp2";

        if [[ "${new_file}" == *.jp2 && ! -f "${new_file}" ]] ;
        then
            continue
            mkdir -p "$(dirname "${new_file}")" && touch "${new_file}";
        fi


        # Run bibdocfile if is REALLY created new jp2 file (check return code)
        convert_image_to_jp "${jpg}" "${new_file}" && append_to_record "${doc_id}" "${new_file}";

    done
}

is_jpylyzer_installed;
list_pdf;
