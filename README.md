# Konvertor PDF -> PDF/A-3b a jp(e)g -> jp2000

## PDF konvertor
## Struktura
..* input_file/ - vstupní soubory, není nutný
..* output_file/ - totéž, pouze výstupní
..* AdobeRGB1998.icc - barevný profil
..* pdf_a2.sh - hlavní skript, obsahuje logiku převodu
..* PDFA_def.ps - postcriptový soubor, popisuje výstupní PDF
..* pdfmarks - ???

## Spuštěnía parametrizace převodu
Skriptu lze zadat jako parametr 
1. soubor -  
```console                                                                                                                         
bash pdf_a2.sh sample.pdf                                                                                                                   
``` 
V tomto případě vytvoří skript ve stejné složce, jako je původní soubor(sample.pdf) soubor sample.archive.pdf
2. adresář -
```console                                                                                                                         
bash pdf_a2.sh /home/user/pdf_to_convert/                                                                                                                   
```
Prohledá celou složku, najde (pouze)PDF soubory a převede a přejmenuje je jako v prvním příkladě
3. soubor soubor - 
```console                                                                                                                         
bash pdf_a2.sh sample.pdf sample2.pdf                                                                                                                   
```
Vezme první soubor, převede ho a uloží jako druhý soubor. Libovolné jméno, libovolné umístění.
4. adresář adresář
```console                                                                                                                         
bash pdf_a2.sh /home/user/pdf_to_convert/ /home/user/pdf_converted/                                                                                                                   
```
Projde PDF soubory v dané složce, převede je a uloží pod stejným jménem do cílové složky.

Pozor na použití stejného adresáře nebo stejného jména při možnosti zadání zdroje a cíle - **přepíše se původní soubor!**

Pro kontrolu zda je vstupní soubor už ve formátu PDF/A je použita velmi jednoduchá funkce využívající pdfinfo.
V podstatě kontroluje pouze meta popis, tzn. nekontroluje zda je validní. To může způsobit problémy, pokud
soubor bude mít v meta popisu PDF/A, ale ve skutečnosti nebude. Bohužel, prozatím je toto jediné řešení bez
použití externích nebo placených knihoven.

## Skript invenio_pdf_a_convertor.sh
Tento skript slouží ke spuštění přímo na stroji s inveniem. Využívá součást invenia bibdocfile. Silně doporučuji 
zkontrolovat cesty k adresářům a nastavit dle skutečnosti. Také by bylo vhodné zkontrolovat a případně přepsat parametry
u bibdocfile podle toho, co chcete (více možností, revize, jména, umístění, ...)

## Poznámky
Celá funkcionalita závisí na Ghostcript a postcript. Do tohoto oboru jsem nijak zvlášť nepronikl, takže by to šlo určitě 
vytvořit lépe někým zkušenějším nebo znalejším GS/PS

## JP2000 konvertor

Tento skript je navržen pro spouštění přímo v prostředí Invenia a využívá jeho nástroje (bibdocfile)
Projde všechny jpg a jpeg soubory v daném adresáři (/opt/invenio/var/data/files/) a vytvoří pro ně kopie 
ve formátu jp2000 a přiloží je k danému záznamu jako nový(další) soubor. Pokud záznam už obsahuje nějaký
jp2000, tak se přeskočí - žádný soubor v tomto záznamu nebude konvertován.