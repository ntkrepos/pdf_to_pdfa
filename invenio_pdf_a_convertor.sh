#!/usr/bin/env bash

append_to_record ()
{
    # Params
    doc_id="${1}";
    pdf_name="${2}";

    # TODO: NEED CHECK!
    # bibdocfile cmd.
    python /opt/invenio/bin/bibdocfile --revise ${pdf_name} --docids=${doc_id} --with-flags='PDF/A' --yes-i-know;
}

list_pdf ()
{
    # Filter only pdf
    all_pdf=($(find /opt/invenio/var/data/files/ -name "*.pdf;*"));
    for pdf in ${all_pdf[@]};
    do

        file_name=$(basename ${pdf});
        doc_id=$(dirname ${pdf} | sed 's#.*/##');
        doc_path=$(dirname ${pdf});
        name=$(python /opt/invenio/bin/bibdocfile --get-info --docids=${doc_id} | grep -Po '(?<=(pdf:name=)).*');

        # Build full path and name
        new_file="/tmp${doc_path}/${name%.*}.pdf";


        cd /vagrant/scripts/pdfa_convertor

        # Run bibdocfile if is REALLY created new pdf file (check return code)
        bash /vagrant/scripts/pdfa_convertor/pdf_a2.sh "${pdf}" "${new_file}" && append_to_record "${doc_id}" "${new_file}";

    done
}

list_pdf